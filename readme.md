REQUIREMENTS

-python3.5 above
-pip3
-mysql
-apache/nginx

INSTALLING MYSQL UBUNTU GUIDE

-apt-get install libmysqlclient-dev

USAGE GUIDE:

create virtualenv wrapper
-virtualenv -p python3 env
-source env/bin/activate
-pip install mysqlclient
-pip install mysql-python
-pip install django_widget_tweaks
-pip install django_settings_export