from django.urls import path
from . import views

app_name = 'core'
urlpatterns = [
	path('login/', views.Login.as_view(), name='login'),
	path('signoff/', views.logout_view, name='logout'),
	path('signup/', views.Signup.as_view(), name='signup'),
	path('success_signup/', views.SuccessSignup.as_view(), name='success_signup'),
	path('activate/<uidb64>/<token>/', views.activate, name='activate_email'),
    path('password/reset/', views.ResetPassword.as_view(), name='password_reset'),
    path('password/change/<token>/', views.ChangePassword.as_view(), name='password_change'),
]