from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.core import serializers
from django.utils import timezone

#generate token
class GenerateToken(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (six.text_type(user.pk) 
            + six.text_type(timestamp)
            + six.text_type(timezone.now())
            + six.text_type(user.email)
        )

generate_token = GenerateToken()

#method for sending/resending email activation
class SendEmail:
	def  __call__(self, request, user, data, subject, template):
	    current_site = get_current_site(request)
	    subject = subject
	    message = render_to_string(template, {
	        'user': user,
	        'data': data,
	        'domain': current_site.domain,
	        'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
	        'token': generate_token.make_token(user),
	    })
	    user.email_user(subject, message)

send_email = SendEmail()

#return json serizlize data
class Json:
	def __call__(self, model):
		data = serializers.serialize("json", model)
		return data

to_json = Json()

